package site.hornsandhooves.dixit.config;

import org.springframework.messaging.Message;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import java.security.Principal;

public class SessionSubscribedEvent extends SessionSubscribeEvent {
    public SessionSubscribedEvent(Object source, Message<?> message)  {
        super(source, (Message<byte[]>) message);
    }

    @Override
    public Principal getUser() {
        return (Principal) super.getMessage().getHeaders().get("simpUser");
    }
}
