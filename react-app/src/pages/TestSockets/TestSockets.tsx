// @ts-nocheck
import { useEffect, useRef, useState } from 'react';
import SockJS from 'sockjs-client'
import webstomp from 'webstomp-client';
import { List, ListItem, Code, Text, Select, Center, Input, Stack, Button, Divider } from '@chakra-ui/react'
import { CheckCircleIcon, TimeIcon } from '@chakra-ui/icons'
import ReactJson from 'react-json-view';
import './TestSockets.scss'




const TestSockets = () => {
    const stompRef = useRef(null)

    const urlRef = useRef("")
    const connectHeaderRef = useRef("")
    const topicAdressRef = useRef("")
    const topicHeadersRef = useRef("")
    const messageAdressRef = useRef("")
    const messageBodyRef = useRef("")
    const messageHeadersRef = useRef("")

    const [connected, setConnected] = useState(false);
    const [responses, setResponses] = useState([])
    const [topics, setTopics] = useState([])

    const JSONHandler = (val) => {
        if (val) {
            return JSON.parse(val)
        } else {
            return {}
        }
    }

    const connectHandler = () => {
        const sock = new SockJS(urlRef.current.value);
        stompRef.current = webstomp.over(sock, { debug: true });
        stompRef.current.connect(JSONHandler(connectHeaderRef.current.value), function (frame) {
            setConnected(true)
            responseHandler(frame)
        });
    }

    const subscribeHandler = () => {
        setTopics(prev => {
            let arr = prev
            arr.unshift(topicAdressRef.current.value)
            return [...arr]
        })
        stompRef.current.subscribe(topicAdressRef.current.value, responseHandler, JSONHandler(topicHeadersRef.current.value));
    }

    const sendHandler = () => {
        stompRef.current.send(messageAdressRef.current.value, messageBodyRef.current.value, JSONHandler(messageHeadersRef.current.value))
    }

    const responseHandler = (resp) => {
        setResponses(prev => {
            let arr = prev
            arr.unshift(resp)
            return [...arr]
        })
    }


    const objects = responses.map((item, i) => <ReactJson src={item} key={i} name="response" />)
    return (
        <Center className='TestSockets__center'>
            <div className='TestSockets__wrapper'>
                <Stack spacing={3}>
                    <div className='TestSockets_send'>
                        <Text>Connect to WS</Text>
                        <Input w={'100%'} placeholder='ws adress' ref={urlRef} size='md' defaultValue={`https://dixit.hornsandhooves.site/gs-guide-websocket?userName=афанасий`} />
                        <Input placeholder='connect headers {"a":"b"}' size='md' ref={connectHeaderRef} />
                        <Button colorScheme='blue' w={'100%'} className='TestSockets_ns' onClick={connectHandler}>connect</Button>
                    </div>
                    <div className='TestSockets_send'>
                        <Text>subscribe to topic</Text>
                        <Input placeholder='topci adress (str)' ref={topicAdressRef} size='md' />
                        <Input placeholder='topci headers {"a":"b"}' ref={topicHeadersRef} size='md' />
                        <Button colorScheme='blue' className='TestSockets_ns' onClick={subscribeHandler}>connect</Button>
                    </div>

                    <div className='TestSockets_send'>
                        <Text >send message</Text>
                        <div className='TestSockets__line'>
                            <Input placeholder='message adress' size='md' ref={messageAdressRef} />
                        </div>
                        <div className='TestSockets__line'>
                            <Input placeholder='message body (str)' size='md' ref={messageBodyRef} />
                            <Input placeholder='message headers {"a":"b"}' size='md' ref={messageHeadersRef} />
                        </div>
                        <Button w={'100%'} colorScheme='blue' className='TestSockets_ns' onClick={sendHandler}>send</Button>

                    </div>
                </Stack>
                <Stack className='TestSockets__json' spacing={3}>
                    {objects}
                </Stack>
            </div>
        </Center>
    )
}

export default TestSockets