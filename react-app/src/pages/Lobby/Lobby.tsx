import { Center, List, ListItem, ListIcon, Button } from '@chakra-ui/react'
import { CheckCircleIcon } from '@chakra-ui/icons'
import {
    Link,
    useNavigate,
    useParams
} from "react-router-dom";
import { useSelector } from 'react-redux';
import { IStore } from '../../redux/store/store';


const Lobby: React.FC = () => {
    let navigate = useNavigate();

    const lobbyID = useSelector((store: IStore) => store.gameInfo.lobbyID)
    const players = useSelector((store: IStore) => store.lobby.players)


    return (
        <Center h='100vh' >
            <div className="lobby__content">
                <p >Ваш gameID: {lobbyID}</p>
                <List spacing={3} mt={10}>
                    {players.map(item => (
                        <ListItem key={item.id}>
                            <ListIcon as={CheckCircleIcon} color='green.500' />
                            {item.name}
                        </ListItem>
                    ))}
                </List>
                <Button mt={4} w={'100%'} colorScheme='blue' onClick={() => { navigate('../game/ddd') }}>Начать игру</Button>
            </div>
        </Center>)
}

export default Lobby