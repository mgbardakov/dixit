import {
    useNavigate
} from "react-router-dom";
import { Center, Input, Button } from '@chakra-ui/react'
import './Auth.scss'
import { useEffect, useState } from "react";
import AuthModal from '../../components/AuthModal/AuthModal'
import { useSelector } from "react-redux";
import { Nullable } from "../../types/types";
import { setName, setCreateGame } from "../../redux/reducers/gameInfoSlice";
import { IStore } from '../../redux/store/store'
import { useAppDispatch } from "../../redux/store/store";







const Auth: React.FC = () => {
    let navigate = useNavigate();
    const [nameState, setNameState] = useState<Nullable<string>>(null)
    const [hasName, setHasName] = useState<boolean>(false)
    const [modalOpen, setModalOpen] = useState<boolean>(false)
    const playerName = useSelector((state: IStore) => state.gameInfo.playerName)


    const dispatch = useAppDispatch()

    useEffect(() => {
        navigate('/')
        nameHandler()
    }, [])


    function nameHandler(): void {
        let Localname: Nullable<string> = localStorage.getItem('playerName')
        if (Localname) {
            setHasName(true)
            dispatch(setName(Localname))
        }
    }

    const nameSubmit = () => {
        dispatch(setName(nameState))
        localStorage.setItem('playerName', String(nameState))
        setHasName(true)

    }
    const createGame = () => {
        dispatch(setCreateGame())

    }

    const openModal = () => {
        setModalOpen(true)
    }
    const authBlock: JSX.Element = <div className="auth__block">
        <Input placeholder='Введите имя' onChange={(e) => { setNameState(e.target.value) }} />
        <Button mt={4} w={'100%'} colorScheme='blue' onClick={nameSubmit}>Подтвердить</Button>
    </div>

    const hasNameBlock: JSX.Element = <div className="auth__block">
        <p>Добро пожаловать, {playerName}</p>
        <Button mt={4} w={'100%'} colorScheme='blue' onClick={createGame}>Создать лобби</Button>
        <Button mt={4} w={'100%'} colorScheme='blue' onClick={openModal}>Войти в лобби</Button>
    </div>


    return (
        <Center h='100vh' >
            <div className="auth">
                {hasName ? hasNameBlock : authBlock}
            </div>
            <AuthModal
                isOpen={modalOpen}
                close={() => { setModalOpen(false) }}
            />
        </Center>
    )
}

export default Auth