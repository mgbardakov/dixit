
// @ts-nocheck
import { useEffect, useRef, useState } from 'react';
import './stomp.scss'
import { useSelector } from 'react-redux';
import SockJS from 'sockjs-client'
import webstomp from 'webstomp-client';
import React from 'react';
import { IStore, useAppDispatch } from '../redux/store/store';
import { setLobbyID } from '../redux/reducers/gameInfoSlice';
import {
    useNavigate
} from "react-router-dom";
import { addPlayers } from '../redux/reducers/lobbySlice';

import { List, ListItem, ListIcon } from '@chakra-ui/react'
import { CheckCircleIcon, TimeIcon } from '@chakra-ui/icons'




const Stomp = () => {
    const [connected, setConnected] = useState(false);
    const [subscribeMesages, setSubscribeMessages] = useState(false)
    const [subscribeQueue, setSubscribeQueue] = useState(false)
    const [subscribeLobby, setSubscribeLobby] = useState(false)
    const [frame, setFrame] = useState({})
    const [counter, setCounter] = useState(0)
    const stompRef = useRef(null)
    let navigate = useNavigate();

    const name = useSelector((state: IStore) => state.gameInfo.playerName)
    const lobbyID = useSelector((state: IStore) => state.gameInfo.lobbyID)
    const createGame = useSelector((state: IStore) => state.gameInfo.createGame)
    const dispatch = useAppDispatch()

    // CONNECT TO WS
    useEffect(() => {
        if (counter == 0 && name) {
            const sock = new SockJS(`https://dixit.hornsandhooves.site/gs-guide-websocket?userName=${name}`);
            stompRef.current = webstomp.over(sock, { debug: true });
            stompRef.current.connect({}, function (frame) {
                setConnected(true)
                setFrame(frame)
            });
            setCounter(1)
        }


    }, [name])


    // SUBSCRIBE TO QUEUE
    useEffect(() => {
        if (connected) {
            stompRef.current.subscribe('/user/' + frame.headers['user-name'] + '/queue/private-messages', messageHandler, {});
        }

    }, [connected])

    // CREATE LOBBY
    useEffect(() => {
        if (subscribeMesages && !lobbyID && createGame) {
            const createL = JSON.stringify({ type: "CREATE_LOBBY" })
            stompRef.current.send('/app/lobby', createL, {})
        }
    }, [subscribeMesages, createGame])

    // JOIN TO LOBBY 

    useEffect(() => {
        if (lobbyID) {
            stompRef.current.subscribe('/topic/lobby/' + lobbyID, lobbyHandler, {});
        }
    }, [lobbyID])


    const messageHandler = (msg) => {
        setSubscribeMessages(true)
        const { payload } = JSON.parse(msg.body)
        if (payload === "CONNECTION_SUCCESS") {
            setSubscribeQueue(true)
        } else {
            dispatch(setLobbyID(payload.lobbyId))
        }
    }

    const lobbyHandler = (msg) => {

        const { payload } = JSON.parse(msg.body)
        console.log(payload);
        navigate("lobby/" + lobbyID);
        setSubscribeLobby(true)
        if (payload.users) {
            dispatch(addPlayers(payload.users))
        }
    }
    return (
        <div className='stomp'>
            <List spacing={1} fontSize={12}>
                <ListItem>
                    {connected ?
                        <ListIcon as={CheckCircleIcon} color='green.500' />
                        : <ListIcon as={TimeIcon} color='red.500' />}

                    {"Подключение к WS"}
                </ListItem>
                <ListItem>
                    {subscribeQueue ?
                        <ListIcon as={CheckCircleIcon} color='green.500' />
                        : <ListIcon as={TimeIcon} color='red.500' />}

                    {"Подкиска на очередь"}
                </ListItem>
                <ListItem>
                    {subscribeLobby ?
                        <ListIcon as={CheckCircleIcon} color='green.500' />
                        : <ListIcon as={TimeIcon} color='red.500' />}

                    {"Подкиска на лобби"}
                </ListItem>
            </List>
        </div>
    )
}

export default Stomp