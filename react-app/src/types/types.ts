export type PlayerType = {
    name: string,
    color: string,
    number: number
}

export type Nullable<T> = T | null;