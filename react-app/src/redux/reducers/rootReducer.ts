import { combineReducers } from "redux";
import gameInfoSlice from "./gameInfoSlice";
import lobbySlice from "./lobbySlice";

export const rootReducer = {
    gameInfo: gameInfoSlice,
    lobby: lobbySlice
}