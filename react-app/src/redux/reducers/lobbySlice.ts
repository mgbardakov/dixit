import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit'
import { Nullable } from "../../types/types";


export type lobbyStateType = {
    players: Array<Player>
}

type Player = {
    host: boolean
    id: string
    name: string
}
export const lobbyInitialState: lobbyStateType = {
    players: []
}

const lobbySlice = createSlice({
    name: 'lobby',
    initialState: lobbyInitialState,
    reducers: {
        addPlayers(state, action: PayloadAction<Array<Player>>) {
            state.players = action.payload
        },

    },
})

export const { addPlayers } = lobbySlice.actions
export default lobbySlice.reducer