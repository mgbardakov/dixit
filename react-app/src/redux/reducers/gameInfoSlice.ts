import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit'
import { Nullable } from "../../types/types";


export type gameInfoStateType = {
  playerName: Nullable<string>,
  lobbyID: Nullable<string>,
  createGame: boolean
}
export const gameInfoinitialState: gameInfoStateType = {
  playerName: "",
  lobbyID: "",
  createGame: false
}

const gameInfo = createSlice({
  name: 'gameInfo',
  initialState: gameInfoinitialState,
  reducers: {
    setName(state, action: PayloadAction<Nullable<string>>) {
      state.playerName = action.payload
    },
    setLobbyID(state, action: PayloadAction<Nullable<string>>) {
      state.lobbyID = action.payload
    },
    setCreateGame(state) {
      state.createGame = true
    }

  },
})

export const { setName, setLobbyID, setCreateGame } = gameInfo.actions
export default gameInfo.reducer