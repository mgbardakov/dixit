import { rootReducer } from '../reducers/rootReducer';
import thunk from 'redux-thunk';
import { configureStore } from '@reduxjs/toolkit'
import { gameInfoinitialState, gameInfoStateType } from '../reducers/gameInfoSlice';
import { useDispatch } from 'react-redux'
import { lobbyInitialState, lobbyStateType } from '../reducers/lobbySlice';

export type IStore = {
    gameInfo: gameInfoStateType
    lobby: lobbyStateType
}

const initialState = {
    gameInfo: gameInfoinitialState,
    lobby: lobbyInitialState
}

export const store = configureStore({
    reducer: rootReducer,
    middleware: [thunk],
    devTools: process.env.NODE_ENV !== 'production',
    preloadedState: initialState
})

export type AppDispatch = typeof store.dispatch
export const useAppDispatch: () => AppDispatch = useDispatch 