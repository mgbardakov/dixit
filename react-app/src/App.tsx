import React from 'react';
import { Dixit, DixitPage } from './pages/Dixit/Dixit';
import Auth from './pages/Auth/Auth';
import Lobby from './pages/Lobby/Lobby';
import Page404 from './pages/Page404/Page404';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
// @ts-ignore
import Stomp from './stomp/Stomp';
import { useSelector } from 'react-redux';
import { IStore } from './redux/store/store';
import TestSockets from './pages/TestSockets/TestSockets';



function App() {

  const lobbyID = useSelector((store: IStore) => store.gameInfo.lobbyID)
  return (


    <Router>
      <Stomp />
      <div>
        <Routes>
          <Route path="/" element={<Auth />}>
          </Route>
          <Route path="game/:gameID" element={<Dixit />}>
          </Route>
          <Route path={'lobby/' + lobbyID} element={<Lobby />}>
          </Route>
          <Route path="test-sockets" element={<TestSockets />}>
          </Route>
          <Route path="*" element={<Auth />}>
          </Route>
        </Routes>
      </div>

    </Router>
  );
}

export default App;
